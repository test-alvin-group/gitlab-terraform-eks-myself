resource "aws_security_group" "elb" {
  name_prefix = "terraform-101-elb-"
  description = "allow all outcomming traffic"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  tags = {
    Name = "terraform-101-elb-sg"
  }
}

resource "aws_security_group" "web" {
  name_prefix = "terraform-101-web-"
  description = "allow all outcomming traffic"

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  tags = {
    Name = "terraform-101-web-sg"
  }
}

resource "aws_security_group_rule" "web-rule" {
  type            = "ingress"
  from_port       = 80
  to_port         = 80
  protocol        = "tcp"

  source_security_group_id = "${aws_security_group.elb.id}"

  security_group_id = "${aws_security_group.web.id}"
}