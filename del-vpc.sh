# According to https://aws.amazon.com/tw/premiumsupport/knowledge-center/troubleshoot-dependency-error-delete-vpc/

#!/bin/bash

# Find the remaining dependencies
vpc="vpc-09791400dd64cfdf0" 
region="ap-northeast-1"
aws ec2 describe-vpc-peering-connections --region $region --filters 'Name=requester-vpc-info.vpc-id,Values='$vpc --profile backyard | grep VpcPeeringConnectionId
aws ec2 describe-nat-gateways --region $region --filter 'Name=vpc-id,Values='$vpc --profile backyard | grep NatGatewayId
aws ec2 describe-instances --region $region --filters 'Name=vpc-id,Values='$vpc --profile backyard | grep InstanceId
aws ec2 describe-vpn-gateways --region $region --filters 'Name=attachment.vpc-id,Values='$vpc --profile backyard | grep VpnGatewayId
aws ec2 describe-network-interfaces --region $region --filters 'Name=vpc-id,Values='$vpc --profile backyard | grep NetworkInterfaceId



# Delete the remaining dependencies
aws ec2 delete-network-interface --network-interface-id eni-071f59cef5e5860d5 --region $region --profile backyard
aws ec2 delete-network-interface --network-interface-id eni-0b2cd5f3040c70edd --region $region --profile backyard