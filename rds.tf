module "cluster-aurora" {
  source  = "terraform-aws-modules/rds-aurora/aws"

  name           = "test-aurora-db-postgres96"
  engine         = "aurora-mysql"
  engine_version = "5.7.mysql_aurora.2.10.2"
 
	instance_class = "db.r5.large"
  instances = {
    one = {}
  }


  create_random_password = true
  random_password_length = 16
  master_username = "admin"
  master_password = "ooii8929!123"


  autoscaling_enabled      = true
  autoscaling_min_capacity = 1
  autoscaling_max_capacity = 5

  vpc_id  = module.vpc.vpc_id

  create_security_group = true

  db_subnet_group_name  = aws_db_subnet_group.postgres_database_subnet_group.name
  vpc_security_group_ids = [aws_security_group.elb.id]


  storage_encrypted   = true
  apply_immediately   = true
  monitoring_interval = 10

  db_parameter_group_name         = "default.aurora-mysql5.7"
  db_cluster_parameter_group_name = "default.aurora-mysql5.7"

  enabled_cloudwatch_logs_exports = ["general"]

  tags = {
    Environment = "production"
    Terraform   = "true"
  }
}