output "cluster_endpoint" {
  description = "Endpoint for EKS control plane."
  value       = module.eks.cluster_endpoint
}


output "cluster_master_password" {
  description = "The database master password"
  value       = try(module.cluster-aurora.master_password, "")
  sensitive   = false
}