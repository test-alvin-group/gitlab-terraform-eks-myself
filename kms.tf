resource "aws_kms_key" "rds" {
  description             = "KMS key 1"
	key_usage                = "ENCRYPT_DECRYPT"
  deletion_window_in_days = 10
}

resource "aws_kms_alias" "rds" {
    name          = "alias/aurora"
    target_key_id = aws_kms_key.rds.key_id
}