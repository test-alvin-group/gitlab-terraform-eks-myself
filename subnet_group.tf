resource "aws_db_subnet_group" "postgres_database_subnet_group" {
  name        = "alvinlin-subnet-group"
  subnet_ids  = module.vpc.private_subnets
}