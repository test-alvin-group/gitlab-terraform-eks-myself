variable "region" {
  default     = "ap-northeast-1"
  description = "AWS region"
}

variable "cluster_name" {
  default     = "gitlab-terraform-eks-alvinlin"
  description = "EKS Cluster name"
}

variable "security_group_name" {
  default     = "gitlab-terraform-eks-security-group-alvinlin"
  description = "EKS Cluster name"
}

variable "cluster_version" {
  default     = "1.21"
  description = "Kubernetes version"
}

variable "instance_name" {
  default     = "eks-gitlab-alvinlin"
  description = "EKS node instance name"
}

variable "instance_type" {
  default     = "t3.small"
  description = "EKS node instance type"
}

variable "instance_count" {
  default     = 3
  description = "EKS node count"
}

variable "agent_namespace" {
  default     = "gitlab-agent"
  description = "Kubernetes namespace to install the Agent"
}

variable "agent_token" {
  description = "Agent token (provided after registering an Agent in GitLab)"
  sensitive   = true
}

variable "kas_address" {
  description = "Agent Server address (provided after registering an Agent in GitLab)"
}
