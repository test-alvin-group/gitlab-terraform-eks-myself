data "aws_kms_secrets" "portal" {
    secret {
        name    = "master_username"
        payload = <<-EOT
          AQICAHgblt+gCaxLkdasZlFCwNo9JQFegI73bkDAQSiRGn+UXAFkeA6g8gc3f367RxZAMYyLAAAAZjBkBgkqhkiG9w0BBwagVzBVAgEAMFAGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQMvWbI5mnvW5FkIwyFAgEQgCNPVv8sH6XBo0hUkEv3frA4CncHbgo+Kceq6tm/M98yuB7BXw==
        EOT
    }

    secret {
        name    = "master_password"
        payload = <<-EOT
          AQICAHgblt+gCaxLkdasZlFCwNo9JQFegI73bkDAQSiRGn+UXAFPhcUBlzKuUG9TEE8EbhrhAAAAZzBlBgkqhkiG9w0BBwagWDBWAgEAMFEGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQMfVQcfS1SdkbtglC0AgEQgCQsmRoGWok69pmv/chyejwjfch289rVmE+4pog9AMSYJTXSW6Y=
        EOT
    }
}